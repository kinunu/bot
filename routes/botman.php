<?php
use App\Http\Controllers\BotManController;
// Don't use the Facade in here to support the RTM API too :)
$botman = resolve('botman');

$botman->hears('test', function($bot){
    $bot->reply('hello!');
});

$botman->hears('invest {user} {amount}', function($bot, $user, $amount){
    $bot->types();
    $bot->reply("Processing your request");
    BotManController::invest($user,$amount);
    $bot->reply("Your Investment of :".$amount.' has been confirmed');
    $bot->reply("Your new balance is :".BotManController::getBal($user));
});

$botman->hears('withdraw {user} {amount}', function($bot, $user, $amount){
    $bot->types();
    $bot->reply("Processing your request");
    $msg = BotManController::withdraw($user,$amount);
    $bot->reply($msg);
    $bot->reply("Your new balance is :".BotManController::getBal($user));
});

$botman->hears('balance {user}', function ($bot, $user) {
    $bot->types();
    $bot->reply("Processing your withdrawal");
    $bal = BotManController::getBal($user);
    $bot->reply("Your Balance is :".$bal);
});

$botman->hears('ref', function ($bot) {
    $user = null;
    $bot->types();
    $msg = BotManController::Referal($user);
    $bot->reply($msg);
});

$botman->hears('history {user}', function ($bot, $user) {
    $bot->types();
    $bot->reply("Fetching your transaction history");
    $msg = BotManController::History($user);
    $bot->reply($msg);
});

$botman->hears('rv {user}', function ($bot, $user) {
    $bot->types();
    $msg = BotManController::reinvest($user);
    $bot->reply($msg);
});


$botman->hears('Start conversation', BotManController::class.'@startConversation');