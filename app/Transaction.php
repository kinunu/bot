<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'user_id', 'type','amount','deposit_confirmed','days_in_contract','bitcoin_wallet_tocken'
    ];
}
